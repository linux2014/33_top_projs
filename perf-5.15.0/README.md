
---

---
### linux perf:
https://zhuanlan.zhihu.com/p/634004893
https://blog.csdn.net/qq_48201696/article/details/126381924
https://zhuanlan.zhihu.com/p/446319798
https://perf.wiki.kernel.org/index.php/Tutorial#Profiling_sleep_times
https://ftp.sjtu.edu.cn/sites/ftp.kernel.org/pub/linux/kernel/tools/perf/v5.15.0/
教程:
https://perf.wiki.kernel.org/index.php/Tutorial#Profiling_sleep_times (在docs目录下);
https://zhuanlan.zhihu.com/p/656795281
https://blog.csdn.net/z1026544682/article/details/115329534
https://blog.csdn.net/NRWHF/article/details/131035384
https://zhuanlan.zhihu.com/p/638791429
https://blog.csdn.net/Xiaoshuai119/article/details/114994627
https://cloud.tencent.com.cn/developer/article/2353822
https://cloud.tencent.com.cn/developer/article/1683074?areaId=106001
https://cloud.tencent.com.cn/developer/article/2245316?areaId=106001
https://cloud.tencent.com.cn/developer/article/1422412?areaId=106001
https://cloud.tencent.com.cn/developer/article/1915640?areaId=106001
https://cloud.tencent.com.cn/developer/article/2353819?areaId=106001
https://www.cnblogs.com/linzhuangrong/articles/17546632.html


### 火焰图+性能分析:
https://cloud.tencent.com/developer/article/2391040
https://zhuanlan.zhihu.com/p/682551503
https://zhuanlan.zhihu.com/p/634004893
https://www.cnblogs.com/biaopei/p/12739474.html
https://cloud.tencent.com/developer/article/1646415
https://cloud.tencent.com/developer/article/2391040


### perf火焰图
https://mp.weixin.qq.com/s/udXNyTd8OIUnIlg-I-KsJg


---
---
perf的依赖安装:
```
sudo apt update
sudo apt install -y flex zlib1g libelf-dev elfutils systemtap-sdt-dev libssl-dev libunwind libdw-dev libcap-dev libzstd-dev binutils-dev libiberty-dev zlib-static libslang2-dev libunwind-dev libperl-dev libnuma-dev libbabeltrace libbabeltrace-dev libbabeltrace-ctf-dev python-dev-is-python3

```
目前在wsl只成功安装了如下的几个依赖:
```
sudo apt install -y flex zlib1g libelf-dev elfutils systemtap-sdt-dev libssl-dev libdw-dev libcap-dev libzstd-dev binutils-dev libiberty-dev libunwind-dev libperl-dev libnuma-dev libbabeltrace-dev libbabeltrace-ctf-dev libslang2-dev


```

### 源码安装perf实例:

https://blog.csdn.net/qq_48201696/article/details/126381924
https://zhuanlan.zhihu.com/p/446319798
```
# 查看自己内核的版本，到官网上去下载特定的内核源码
>$ uname -r
5.4.0-122-generic
# 去官网上下载内核源码，可以手动下载，也可以使用wget
>$ wget http://ftp.sjtu.edu.cn/sites/ftp.kernel.org/pub/linux/kernel/v5.x/linux-5.4.122.tar.gz
# 下载完毕之后，接下内核源代码
>$ tar -zxvf linux-5.4.122.tar.gz
# 进入如下目录
>$ cd linux-5.4.122/tools/perf/
# 源码级安装, 如有些依赖包没有安装，得安装一下，依赖包在下面第二个链接
>$ make -j10 && make install
# 查看perf的安装情况
>$ perf --version
perf version 5.4.192

```

##### 指定安装目录的方法可参考makefile文件:
```
Makefile.perf:276:export prefix bindir sharedir sysconfdir DESTDIR
Makefile.perf:698:# If a target does not match any of the later rules then prefix it by $(OUTPUT)
Makefile.perf:865:      @echo '  HINT: use "prefix" or "DESTDIR" to install to a particular'
Makefile.perf:866:      @echo '        path like "make prefix=/usr/local install install-doc"'
```

---
---
---
---
---

---



