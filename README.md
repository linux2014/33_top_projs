


---

## linux性能监控和分析

## 问题案例:
```
磁盘IO的案例
网络IO的案例
网络带宽问题
```



---

### perf
perf-5.15.0

---
### linux perf:
https://zhuanlan.zhihu.com/p/138502887
https://zhuanlan.zhihu.com/p/682551503
https://zhuanlan.zhihu.com/p/680152094
https://zhuanlan.zhihu.com/p/634004893
https://blog.csdn.net/qq_48201696/article/details/126381924
https://zhuanlan.zhihu.com/p/446319798
https://perf.wiki.kernel.org/index.php/Tutorial#Profiling_sleep_times
https://ftp.sjtu.edu.cn/sites/ftp.kernel.org/pub/linux/kernel/tools/perf/v5.15.0/
教程:
https://perf.wiki.kernel.org/index.php/Tutorial#Profiling_sleep_times (在docs目录下);
https://zhuanlan.zhihu.com/p/656795281
https://blog.csdn.net/z1026544682/article/details/115329534
https://blog.csdn.net/NRWHF/article/details/131035384
https://zhuanlan.zhihu.com/p/638791429
https://blog.csdn.net/Xiaoshuai119/article/details/114994627

#### perf的依赖安装:
```
sudo apt update
sudo apt install -y flex zlib1g libelf-dev elfutils systemtap-sdt-dev libssl-dev libunwind libdw-dev libcap-dev libzstd-dev binutils-dev libiberty-dev zlib-static libslang2-dev libunwind-dev libperl-dev libnuma-dev libbabeltrace libbabeltrace-dev libbabeltrace-ctf-dev python-dev-is-python3

```
目前在wsl只成功安装了如下的几个依赖:
```
sudo apt install -y flex zlib1g libelf-dev elfutils systemtap-sdt-dev libssl-dev libdw-dev libcap-dev libzstd-dev binutils-dev libiberty-dev libunwind-dev libperl-dev libnuma-dev libbabeltrace-dev libbabeltrace-ctf-dev libslang2-dev


```

#### perf源码构建:

进入目录perf-5.15.0/tools/perf,执行如下命令:
```
make -j8 && make install 
```
将生成的perf*可执行文件放入指定的目录下,在构建目录下执行make clean;

```
cp -R $(ls perf*) /mnt/d/D/linux_install_dir/perf_v515/bin/
make clean
```

---

### 闻茂泉：系统性能监控与分析的工程化实践之路
https://cloud.tencent.com/developer/article/1942107

### 超全整理！Linux 性能分析工具汇总
https://cloud.tencent.com/developer/article/2142516?areaId=106001


---

### linux性能监控:

perf,ftrace,strace,collectl:
https://www.sohu.com/a/453814770_120888633
https://developer.aliyun.com/article/1116211
https://zhuanlan.zhihu.com/p/582108372
https://developer.aliyun.com/article/1116211

IT项目研发过程中的利器——C/C++项目调用图篇
https://fangliang.blog.csdn.net/article/details/135971707

动态执行流程分析和性能瓶颈分析的利器——gperftools的Cpu Profiler
https://blog.csdn.net/breaksoftware/article/details/81315729?utm_medium=distribute.pc_relevant.none-task-blog-2~default~baidujs_baidulandingword~default-9-81315729-blog-83820080.235^v43^pc_blog_bottom_relevance_base2&spm=1001.2101.3001.4242.6&utm_relevant_index=12
动态分析C语言代码生成函数调用关系的利器——gprof
https://fangliang.blog.csdn.net/article/details/135939000

---
#### perf+gprof2dot.py+dot生成函数调用关系的案例:
```
perf record -g -- ./test-time
perf script > test-time-perf.output
gprof2dot test-time-perf.output -f perf> test-time-per.dot
dot test-time-perf.dot -Tpng -o test-time-perf.png
```

```
perf script | gprof2dot.py -f perf | dot -Tpng -o 3_5.png
```

### perf生成火焰图案例:
https://www.cnblogs.com/linzhuangrong/articles/17546632.html
```
dd if=/dev/zero of=/tmp/testfile bs=4K count=102400 &
perf record -e cpu-cycles -a -g sleep 1
perf script > perf.unfold
cd FlameGraph-master
./stackcollapse-perf.pl < ../perf.unfold | ./flamegraph.pl > ../perf.svg
```

---

动态分析C语言代码生成函数调用关系的利器——perf
https://fangliang.blog.csdn.net/article/details/135948276?ydreferer=aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2JyZWFrc29mdHdhcmUvYXJ0aWNsZS9kZXRhaWxzLzgxMzE1NzI5P3V0bV9tZWRpdW09ZGlzdHJpYnV0ZS5wY19yZWxldmFudC5ub25lLXRhc2stYmxvZy0yfmRlZmF1bHR%2BYmFpZHVqc19iYWlkdWxhbmRpbmd3b3JkfmRlZmF1bHQtOS04MTMxNTcyOS1ibG9nLTgzODIwMDgwLjIzNV52NDNecGNfYmxvZ19ib3R0b21fcmVsZXZhbmNlX2Jhc2UyJnNwbT0xMDAxLjIxMDEuMzAwMS40MjQyLjYmdXRtX3JlbGV2YW50X2luZGV4PTEy




程序员眼中的linux:

https://github.com/judasn/Linux-Tutorial

---


btop,bashtop;
top:

https://zhuanlan.zhihu.com/p/641278101
https://blog.csdn.net/sinat_28442665/article/details/126346052
https://blog.csdn.net/langzi6/article/details/124805024
https://blog.csdn.net/weixin_44758876/article/details/117463687?spm=1001.2101.3001.6650.1&utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7ECTRLIST%7ERate-1-117463687-blog-126346052.235%5Ev38%5Epc_relevant_sort_base2&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7ECTRLIST%7ERate-1-117463687-blog-126346052.235%5Ev38%5Epc_relevant_sort_base2&utm_relevant_index=2
https://blog.csdn.net/chshgod1/article/details/130343184?spm=1001.2101.3001.6650.13&utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7ERate-13-130343184-blog-126346052.235%5Ev38%5Epc_relevant_sort_base2&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7ERate-13-130343184-blog-126346052.235%5Ev38%5Epc_relevant_sort_base2&utm_relevant_index=20
https://huaweicloud.csdn.net/63561a18d3efff3090b5a432.html?spm=1001.2101.3001.6650.5&utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7Eactivity-5-125055298-blog-130343184.235%5Ev38%5Epc_relevant_sort_base2&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7Eactivity-5-125055298-blog-130343184.235%5Ev38%5Epc_relevant_sort_base2&utm_relevant_index=8
https://blog.csdn.net/weixin_44988085/article/details/108637703?spm=1001.2101.3001.6650.12&utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7ERate-12-108637703-blog-130343184.235%5Ev38%5Epc_relevant_sort_base2&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7ERate-12-108637703-blog-130343184.235%5Ev38%5Epc_relevant_sort_base2&utm_relevant_index=15
https://zhuanlan.zhihu.com/p/579726995?utm_id=0

---

Sar:
https://zhuanlan.zhihu.com/p/539167022
https://blog.csdn.net/Chiang2018/article/details/121409050
https://blog.csdn.net/weixin_34349320/article/details/88860206?spm=1001.2101.3001.6661.1&utm_medium=distribute.pc_relevant_t0.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7ERate-1-88860206-blog-121409050.235%5Ev38%5Epc_relevant_sort_base2&depth_1-utm_source=distribute.pc_relevant_t0.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7ERate-1-88860206-blog-121409050.235%5Ev38%5Epc_relevant_sort_base2&utm_relevant_index=1
https://www.linuxprobe.com/sar-ksar-bottleneck.html
sar配置文件:https://www.cnblogs.com/mikeguan/p/6371278.html

Ftrace:
https://zhuanlan.zhihu.com/p/479833554


nmon:
https://blog.csdn.net/m0_61066945/article/details/126799593

nmon:
https://zhuanlan.zhihu.com/p/143219027
https://zhuanlan.zhihu.com/p/392911491
https://zhuanlan.zhihu.com/p/614983019
njmon:
https://zhuanlan.zhihu.com/p/116038277

linux性能排查:
https://zhuanlan.zhihu.com/p/68221840

glances:
https://zhuanlan.zhihu.com/p/534563140
https://blog.csdn.net/hzblucky1314/article/details/129959654
https://blog.csdn.net/weixin_42545159/article/details/116636638?utm_medium=distribute.pc_relevant.none-task-blog-2~default~baidujs_baidulandingword~default-1-116636638-blog-129959654.235^v38^pc_relevant_sort_base2&spm=1001.2101.3001.4242.2&utm_relevant_index=4
https://blog.csdn.net/weixin_45426964/article/details/106072522?spm=1001.2101.3001.6650.2&utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7ERate-2-106072522-blog-116636638.235%5Ev38%5Epc_relevant_sort_base2&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7ERate-2-106072522-blog-116636638.235%5Ev38%5Epc_relevant_sort_base2&utm_relevant_index=5

Linux服务器监控神器：Netdata安装、使用
https://mp.weixin.qq.com/s?__biz=Mzg5NzEwMzkxNw==&mid=2247487363&idx=3&sn=b3f19c8b6f6ca0d5a1a9aecc1520eec1&chksm=c077aec1f70027d7ac8b05a487f4382050b055ea8f53fe2470ba6e5e8a2d4bb7f9a4ab461066&scene=21#wechat_redirect


netdata:
https://mp.weixin.qq.com/s?__biz=Mzg5NzEwMzkxNw==&mid=2247487363&idx=3&sn=b3f19c8b6f6ca0d5a1a9aecc1520eec1&chksm=c077aec1f70027d7ac8b05a487f4382050b055ea8f53fe2470ba6e5e8a2d4bb7f9a4ab461066&scene=21#wechat_redirect

https://github.com/netdata/netdata  
https://github.com/coraxx/netdata_nv_plugin  


iostat
dmesg 
----

## 项目实战:
java,spring项目:
https://github.com/ynwynw/allProject

极客时间错误代码研究:
https://github.com/JosephZhu1983/java-common-mistakes

中国独立开发者项目列表
https://github.com/1c7/chinese-independent-developer

自动化测试:
https://github.com/SeleniumHQ/selenium
https://github.com/Genymobile/scrcpy


blockchain:
https://github.com/1uvu/Blockchain-Notes



https://github.com/davecheney/httpstat
https://github.com/reorx/httpstat
教程:
https://github.com/dashpradeep99/https-github.com-miguellgt-books



---

io案例:
https://zhuanlan.zhihu.com/p/579187444?utm_id=0
https://developer.aliyun.com/article/1228095
https://www.cnblogs.com/mys6/p/14941914.html
https://blog.csdn.net/n88Lpo/article/details/124335218
https://blog.csdn.net/chen250727498/article/details/125597496
https://www.cnblogs.com/wyf0518/p/12213999.html


https:
https://www.cnblogs.com/zjdxr-up/p/14359904.html
https://juejin.cn/post/6844903977025273863
https://zhuanlan.zhihu.com/p/347527635
https://blog.csdn.net/zhanglong_longlong/article/details/111319505
SSL证书:
https://zhuanlan.zhihu.com/p/625650319
https://baijiahao.baidu.com/s?id=1749468274828466756&wfr=spider&for=pc
https://baijiahao.baidu.com/s?id=1763770305773579491&wfr=spider&for=pc
https://zhuanlan.zhihu.com/p/377352148
https://zhuanlan.zhihu.com/p/88043684
查看本机的网络设备;

---
---

cpu的性能指标:
https://baijiahao.baidu.com/s?id=1748361519766065643
https://www.zhihu.com/tardis/zm/art/664689886?source_id=1005
https://www.zhihu.com/question/566216603#:~:text=%E5%BE%AE%E5%9E%8B%E8%AE%A1%E7%AE%97%E6%9C%BACPU%E7%9A%84%E4%B8%BB%E8%A6%81%20%E6%8A%80%E6%9C%AF%E6%8C%87%E6%A0%87%20%E5%8C%85%E6%8B%AC%E5%AD%97%E9%95%BF%E3%80%81%E6%97%B6%E9%A2%91%E7%8E%87%E3%80%81%E8%BF%90%E7%AE%97%E9%80%9F%E5%BA%A6%E3%80%81%20%E5%AD%98%E5%82%A8%E5%AE%B9%E9%87%8F%20%E3%80%81%E5%AD%98%E5%8F%96%E5%91%A8%E6%9C%9F%E7%AD%89%E3%80%82%20CPU%E7%9A%84%E9%A2%91%E7%8E%87%E6%98%AF%E6%8C%87%E5%85%B6%E5%B7%A5%E4%BD%9C%E9%A2%91%E7%8E%87%EF%BC%8C%E5%88%86%E4%B8%BA%20%E4%B8%BB%E9%A2%91%20%E3%80%81%E5%A4%96%E9%A2%91%E5%92%8C%E5%80%8D%E9%A2%91%E3%80%82,%E7%B3%BB%E7%BB%9F%E6%80%BB%E7%BA%BF%20%E7%9A%84%E5%B7%A5%E4%BD%9C%E9%A2%91%E7%8E%87%EF%BC%8C%E5%8D%B3CPU%E7%9A%84%20%E5%9F%BA%E5%87%86%E9%A2%91%E7%8E%87%20%EF%BC%8C%E6%98%AFCPU%E4%B8%8E%E4%B8%BB%E6%9D%BF%E4%B9%8B%E9%97%B4%E5%90%8C%E6%AD%A5%E8%BF%90%E8%A1%8C%E7%9A%84%E9%80%9F%E5%BA%A6%E3%80%82%20%E5%A4%96%E9%A2%91%E9%80%9F%E5%BA%A6%E8%B6%8A%E9%AB%98%EF%BC%8CCPU%E5%B0%B1%E5%8F%AF%E4%BB%A5%E5%90%8C%E6%97%B6%E6%8E%A5%E5%8F%97%E6%9B%B4%E5%A4%9A%E6%9D%A5%E8%87%AA%E5%A4%96%E5%9B%B4%E8%AE%BE%E5%A4%87%E7%9A%84%E6%95%B0%E6%8D%AE%EF%BC%8C%E4%BB%8E%E8%80%8C%E4%BD%BF%E6%95%B4%E4%B8%AA%E7%B3%BB%E7%BB%9F%E7%9A%84%E9%80%9F%E5%BA%A6%E8%BF%9B%E4%B8%80%E6%AD%A5%E6%8F%90%E9%AB%98%E3%80%82%203%E3%80%81%E5%80%8D%E9%A2%91%E5%88%99%E6%98%AF%E6%8C%87%20CPU%E5%A4%96%E9%A2%91%20%E4%B8%8E%E4%B8%BB%E9%A2%91%E7%9B%B8%E5%B7%AE%E7%9A%84%E5%80%8D%E6%95%B0%E3%80%82
主频,外频,字长,物理核心数,缓存;

Linux查看物理CPU个数、核数、逻辑CPU个数，以及内存
https://zhuanlan.zhihu.com/p/372564248
https://mp.weixin.qq.com/s?__biz=MzI4ODE0NTE3OA==&mid=2649213948&idx=1&sn=d739057afe49dca096fd1ce4ddb16a6d&chksm=f3d18189c4a6089f3071fb2aad5fa5b38ac8d1a12914677a3fd815255445176e7b598d51d18e&token=2110506281&lang=zh_CN#rd
https://www.tecmint.com/commands-to-collect-system-and-hardware-information-in-linux/
https://zhuanlan.zhihu.com/p/617615297?utm_id=0

```
/proc/cpuinfo,/proc/meminfo,
lshw 
lshw -html > lshw.html
lsblk
```

linux下的硬盘分区
分区创建,查看,挂载,卸载;
https://zhuanlan.zhihu.com/p/560202836?utm_id=0
https://zhuanlan.zhihu.com/p/549994779?utm_id=0
Linux fdisk命令详解
https://c.biancheng.net/view/891.html
https://www.cnblogs.com/machangwei-8/p/10353683.html
https://blog.csdn.net/weixin_46560589/article/details/126465198
如何在 Linux 上检查网卡信息
https://zhuanlan.zhihu.com/p/137332390

---

### java/jdk的性能分析工具
https://my.oschina.net/liux/blog/51800

---


---
---
---
---
---
